# JZJCoreE

My fifth RISC-V soft core cpu implementation (RV32IZifencei) with an fMax that is even higher!

I am making this core design after having finished JZJPipelinedCoreA. I thought of some ideas while making that pipelined core and I'm going to try to apply them here. I don't expect this core to outperform JZJPipelinedCoreA for 2 cycle instructions, but I hope it will have a higher Fmax than JZJCoreC (ended up being around 0.something to 3 mhz faster; negligible). The CPI will be the same as JZJCoreC because most of the code is the same; there will just be some fixes and some optimizations.

I'll be using code from both JZJPipelinedCoreA and JZJCoreC, and then I can use code from this cpu in JZJPipelinedCoreB. Changes here include turning lots of 0 assignments for things that don't matter into don't cares instead, changing MemoryMappedIOManager to use regular inputs and outputs instead of inouts (moving the responsibility of synchronizers and tristate to external modules), and other things I find.

Not super polished, but I think a later single cycle cpu (either F or G) will be completely written (only refrencing old core code for some ideas rather than copy-pasting). I also might rewrite the pipelined core at some point too. JZJCoreF might end up being a longer term core and I'll stop constantly forking like I'm doing an use git tags/versioning more.

## Cycle counts for instructions

Note: The first instruction executed takes an additional cycle because it needs to be fetched from memory. After that, the next instruction is fetched during the execution of the one that was previously fetched.

Some instructions take two cycles so the fetch for the next instruction is performed on the second cycle of the two cycle instruction.

| Instruction | Cycle Count |
|:------------|:-----------------------|
|-Base Spec(I)-|
| lui | 1 |
| auipc | 1 |
| jal | 1 |
| jalr | 1 |
| beq | 1 |
| bne | 1 |
| blt | 1 |
| bge | 1 |
| bltu | 1 |
| bgeu | 1 |
| lb | 2 |
| lh | 2 |
| lw | 2 |
| lbu | 2 |
| lhu | 2 |
| sb | 2 |
| sh | 2 |
| sw | 1 |
| addi | 1 |
| slti | 1 |
| sltiu | 1 |
| xori | 1 |
| ori | 1 |
| andi | 1 |
| slli | 1 |
| srli | 1 |
| srai | 1 |
| add | 1 |
| sub | 1 |
| sll | 1 |
| slt | 1 |
| sltu | 1 |
| xor | 1 |
| srl | 1 |
| sra | 1 |
| or | 1 |
| and | 1 |
| fence | 1 |
| ecall | 1 |
| ebreak | 1 |
|-Zifencei-|
| fence.i | 1 |

## Memory Map

Note: addresses are inclusive, bounds not specified are not addressed to anything, and execution starts at 0x00000000

Note: MMIO registers work differently now; there are no direction registers anymore, only port registers

Note: Writes to out of bounds addresses have undefined behaviour; instructions can only be fetched from addresses between RAM Start and RAM End

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003FFF|RAM End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
