module RegisterFile
(
	input clock,
	input reset,
	
	//Writing
	input [31:0] dataIn,
	input [4:0] rd,
	input writeEnable,
	
	//Read ports
	input [4:0] rs1,
	input [4:0] rs2,
	output [31:0] portRS1,
	output [31:0] portRS2,
	
	//Output for legacy asembly test programs that output to register 31; for new software use memory mapped io instead
	output [31:0] register31Output
);
/* Registers and Assignments */

//The actual register file
reg [31:0] actualRegisterFile [31:1];//x0 is never written to so there is no reason to dedicate an actual register to it

//Reading ports for portRS1, portRS2 and register31Output
assign portRS1 = (rs1 == 5'b00000) ? 32'h00000000 : actualRegisterFile[rs1];//x0 is always 32'h00000000
assign portRS2 = (rs2 == 5'b00000) ? 32'h00000000 : actualRegisterFile[rs2];//x0 is always 32'h00000000

assign register31Output = actualRegisterFile[31];//Output for legacy asembly test programs that output to register 31; for new software use memory mapped io instead

/* Writing logic */

always @(posedge clock, posedge reset)
begin
	if (reset)
		initRegisters();
	else if (clock)
	begin
		if (writeEnable & (rd != 5'b00000))//Write the appropriate register, ignoring writes to x0
			actualRegisterFile[rd] <= dataIn;//read new data off the bus into the register
	end
end

/* Register Initialization */

//Initialize registers at powerup
initial
begin
	initRegisters();
end

//Sets all registers to 0
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and reset inside of the write block
integer i;
begin
	//Initialze registers to 0
	for (i = 1; i < 32; i = i + 1)
	begin
		actualRegisterFile[i] <= 32'h00000000;
	end
end
endtask

endmodule