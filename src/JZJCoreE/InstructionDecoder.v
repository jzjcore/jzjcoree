module InstructionDecoder//splits up instruction into parts and properly formats immediates
(
	input [31:0] instruction,
	
	//Note: not all of these will be valid at a given instant
	//It is expected the module/control logic knows which instruction type
	//is being used and therefore which of these is valid
	//This alows for a minimal amount of logic in this module
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode,//Not used in JZJCoreE
	
	//Immediates (already processed)
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ
);
/* Wires and Assignments */

assign funct7 = instruction[31:25];
assign rs2 = instruction[24:20];
assign rs1 = instruction[19:15];
assign funct3 = instruction[14:12];
assign rd = instruction[11:7];
assign opcode = instruction[6:0];//Not used in JZJCoreE

//Immediates
//todo use bit extension functions instead/move 12 bit extension to BitExtensionFunctions.v

wire [11:0] immediateILow12Bits = instruction[31:20];
assign immediateI = signExtend12To32(immediateILow12Bits);

wire [11:0] immediateSLow12Bits = {instruction[31:25], instruction[11:7]};
assign immediateS = signExtend12To32(immediateSLow12Bits);

wire [12:0] immediateBLow13Bits = {instruction[31], instruction[7], instruction[30:25], instruction[11:8], 1'b0};//0 out bottom bit because only multiples of 2 are allowed
assign immediateB = signExtend13To32(immediateBLow13Bits);

assign immediateU[31:12] = instruction[31:12];
assign immediateU[11:0] = 12'b000000000000;//both lui and auipc zero out the bottom 12 bits, so we do that here

wire [20:0] immediateJLow21Bits = {instruction[31], instruction[19:12], instruction[20], instruction[30:21], 1'b0};//0 out bottom bit because only multiples of 2 are allowed
assign immediateJ = signExtend21To32(immediateJLow21Bits);

/* External Stuff */

//Bit extension functions
`include "JZJCoreE/BitExtensionFunctions.v"

endmodule