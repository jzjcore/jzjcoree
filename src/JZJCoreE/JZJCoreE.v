module JZJCoreE
#(
	parameter INITIAL_MEM_CONTENTS = "initialRam.mem",//File containing initial ram contents (32 bit words); execution starts from address 0x00000000
	parameter RAM_A_WIDTH = 12//number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes
)
(
	input clock,
	input reset,
	
	//CPU memory mapped ports
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portXInput[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portXOutput[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Reads from the address read from the input, writes write to the output
	//Inputs: (byte-wise read)		address (starting byte)
	input [31:0] portAInput,//		FFFFFFE0
	input [31:0] portBInput,//		FFFFFFE4
	input [31:0] portCInput,//		FFFFFFE8
	input [31:0] portDInput,//		FFFFFFEC
	input [31:0] portEInput,//		FFFFFFF0
	input [31:0] portFInput,//		FFFFFFF4
	input [31:0] portGInput,//		FFFFFFF8
	input [31:0] portHInput,//		FFFFFFFC
	//Outputs: (word-wise write)	address (starting byte)
	output [31:0] portAOutput,//	FFFFFFE0
	output [31:0] portBOutput,//	FFFFFFE4
	output [31:0] portCOutput,//	FFFFFFE8
	output [31:0] portDOutput,//	FFFFFFEC
	output [31:0] portEOutput,//	FFFFFFF0
	output [31:0] portFOutput,//	FFFFFFF4
	output [31:0] portGOutput,//	FFFFFFF8
	output [31:0] portHOutput,//	FFFFFFFC
	//For tristate ports, an additional port's outputs can be designated as a direction register, which can be used by and external module to allow/disalow writing
	//If feedback is desired, then inputs should be connected to their respective output register
	//MAKE SURE INPUTS ARE SYNCHRONIZED IF THEY ARE FROM ANOTHER CLOCK DOMAIN
	
	//Output for legacy asembly test programs that output to register 31; for new software use memory mapped io instead
	output [31:0] register31Output
);
/* Wires, Registers, and Assignments */

//Register inputs and or gate logic (instead of a central, tri-state bus)
wor [31:0] registerFileIn;//wor because several modules might want to write to registers
wire [31:0] programCounterIn;//wire because only PCALU will be driving it
wire [31:0] instructionIn;//used for instruction register inside of the control logic; wire because only memory will be driving it

//Module outputs and register input assignment
//Outputs
wire [31:0] aluResult;
wire [31:0] valueFormerValue;
wire [31:0] pcALURDOut;
wire [31:0] memoryDataOut;
//Assignments
assign registerFileIn = aluResult;
assign registerFileIn = valueFormerValue;
assign registerFileIn = pcALURDOut;
assign registerFileIn = memoryDataOut;

//Register outputs
wire [31:0] portRS1;
wire [31:0] portRS2;
wire [31:0] pcOut;//Pointer to the current memory address

//Control lines
//Register file
wire registerFileWE;//Latch new data into register rd at next posedge
//ALU
wire aluOE;//Output alu result to registerFileIn
wire opImm;//Whether the opcode type is opImm (1) or op (0)
//Value former
wire valueFormerOE;//Output formed value to registerFileIn
wire valueFormerOperation;//Whether to output the value for a lui (0) is auipc (1) instruction
//Program counter
wire pcWE;
//Program counter ALU
wire pcALUOE;
wire [1:0] pcALUOperation;
//Memory
wire memoryEnable;
wire [1:0] memoryOperation;

//Bypasses/instruction addressing for control logic
wire [31:0] instructionAddressToAccess;//instruction address to access through instructionOut port
wire [31:0] nextPC = programCounterIn;//the address of the next pc to be latched at the next posedge (allows setting instructionaddressToAccess early); this is actually always output but only latched into pc when pcWE is enabled, so we can leach off of it at any time instruction decode is valid

//Decoded instruction output
//Note: even if these change, module shouldn't do anything to their cpu visible state
//Modules should look at these in response to a control line change (because the control
//logic interprets the opcode)
wire [6:0] funct7;
wire [4:0] rs2;
wire [4:0] rs1;
wire [2:0] funct3;
wire [4:0] rd;
wire [31:0] immediateI;
wire [31:0] immediateS;
wire [31:0] immediateB;
wire [31:0] immediateU;
wire [31:0] immediateJ;
//Source (what is sent to the instruction decoder to produce these outputs)
wire [31:0] instructionNoOpcode;

//Error flags
wire pcMisaligned;//program counter is not aligned to a 4 byte address
wire jumpMisaligned;//new program counter address is not aligned to a 4 byte address
wire memoryAccessMisaligned;//memory access is not aligned to a 4 byte address for a word or a 2 byte address for a half word
wor badInstruction;//bad encoding (funct3, funct7)
//badInstruction flags
wire pcALUBadFunct3;
wire memoryBadFunct3;
//badInstruction wor assignment
assign badInstruction = pcALUBadFunct3;
assign badInstruction = memoryBadFunct3;

/* CPU Modules */

//Register file
RegisterFile registerFile (.clock(clock), .reset(reset), .dataIn(registerFileIn), .rd(rd), .writeEnable(registerFileWE), .rs1(rs1), .rs2(rs2), .portRS1(portRS1), .portRS2(portRS2),
									.register31Output(register31Output));

//ALU						
ALU alu (.resultOut(aluResult), .outputEnable(aluOE), .portRS1(portRS1), .portRS2(portRS2), .immediateI(immediateI), .funct3(funct3), .opImm(opImm), .funct7(funct7));

//Value former
ValueFormer valueFormer (.valueOut(valueFormerValue), .outputEnable(valueFormerOE), .operation(valueFormerOperation), .pc(pcOut), .immediateU(immediateU));

//Program counter
ProgramCounter programCounter (.clock(clock), .reset(reset), .writeEnable(pcWE), .newPCIn(programCounterIn), .pcOut(pcOut), .misaligned(pcMisaligned));

//Program counter ALU
PCALU programCounterALU (.newPCOut(programCounterIn), .rdOut(pcALURDOut), .invalidFunct3(pcALUBadFunct3), .jumpMisaligned(jumpMisaligned), .outputEnable(pcALUOE),
								 .operation(pcALUOperation), .portRS1(portRS1), .portRS2(portRS2), .funct3(funct3), .immediateJ(immediateJ), .immediateI(immediateI), .immediateB(immediateB),
								 .pcIn(pcOut));

//Memory
MemoryController #(.INITIAL_CONTENTS(INITIAL_MEM_CONTENTS), .A_WIDTH(RAM_A_WIDTH)) memoryController
						(.clock(clock), .reset(reset), .dataOut(memoryDataOut), .unalignedAccess(memoryAccessMisaligned), .invalidFunct3(memoryBadFunct3), .instructionOut(instructionIn),
						 .instructionAddress(instructionAddressToAccess), .memOperationType(memoryOperation), .funct3(funct3), .enable(memoryEnable), .portRS1(portRS1), .portRS2(portRS2),
						 .immediateI(immediateI), .immediateS(immediateS), .portAInput(portAInput), .portBInput(portBInput), .portCInput(portCInput), .portDInput(portDInput),
						 .portEInput(portEInput), .portFInput(portFInput), .portGInput(portGInput), .portHInput(portHInput), .portAOutput(portAOutput), .portBOutput(portBOutput),
						 .portCOutput(portCOutput), .portDOutput(portDOutput), .portEOutput(portEOutput), .portFOutput(portFOutput), .portGOutput(portGOutput), .portHOutput(portHOutput));

//Control logic
ControlLogic controlLogic (.clock(clock), .reset(reset), .registerFileWE(registerFileWE), .aluOE(aluOE), .opImm(opImm), .valueFormerOE(valueFormerOE),
									.valueFormerOperation(valueFormerOperation), .pcWE(pcWE), .pcALUOE(pcALUOE), .pcALUOperation(pcALUOperation), .memoryEnable(memoryEnable),
									.memoryOperation(memoryOperation), .pcMisaligned(pcMisaligned), .jumpMisaligned(jumpMisaligned), .memoryAccessMisaligned(memoryAccessMisaligned),
									.badInstruction(badInstruction), .instructionIn(instructionIn), .instructionAddressToAccess(instructionAddressToAccess),
									.instructionOutNoOpcode(instructionNoOpcode), .funct3(funct3), .pcIn(pcOut), .nextPC(nextPC));

//Instruction decoder
InstructionDecoder instructionDecoder (.instruction(instructionNoOpcode), .funct7(funct7), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .immediateI(immediateI),
													.immediateS(immediateS), . immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ));

endmodule
