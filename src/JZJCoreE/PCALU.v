module PCALU//Updates the pc to the next instruction (either +4 or something else for jumps/branches)
(
	//Output
	output reg [31:0] newPCOut,
	output [31:0] rdOut,
	output invalidFunct3,
	output jumpMisaligned,
	input outputEnable,//enables rdOut; newPCOut is always active but this should still be enabled for increments so error flags are output...
	//...This way operation can be assigned a don't care (2'bxx) without possibly halting the core
	
	//Operation type
	input [1:0] operation,//0 indicates jal, 1 indicates jalr, 2 indicates beq/bne//blt/bge/bltu/bgeu, and 3 indicates increment pc to the next address (pcIn + 4)
	
	//Data sources (registers, immediates and the current program counter value)
	input [31:0] portRS1,//for branches and jalr
	input [31:0] portRS2,//for branches (used in comparisons)
	input [2:0] funct3,//decide between beq/bne/blt/bge/bltu/bgeu; for jal and inc this is ignored, for jalr this should be 3'b000
	input [31:0] immediateJ,//jal immediate
	input [31:0] immediateI,//jalr immediate
	input [31:0] immediateB,//immediate type for branch instructions
	input [31:0] pcIn//address of current instruction
);
/* Wires, Registers, and Assignments */

reg branchTaken;//Branch is going to be taken or not
reg [31:0] rdToOutput;//Only output if outputEnable 
reg invalidFunct3ToOutput;
wire jumpMisalignedToOutput = (newPCOut % 4) != 0;//new pc is not aligned to a 4 byte address

assign rdOut = outputEnable ? rdToOutput : 32'h00000000;
assign invalidFunct3 = outputEnable ? invalidFunct3ToOutput : 1'b0;
assign jumpMisaligned = outputEnable ? jumpMisalignedToOutput : 1'b0;

//Helper wires
wire [31:0] nextInstruction = pcIn + 4;//the instruction in memory immedatly following this one (not necessarly the one sent to newPCOut)
wire [31:0] jumpRD = nextInstruction;//jal and jalr both put the address of the instruction after them into the rd register
wire [31:0] branchFailNewPC = nextInstruction;//if a branch condition is not satasfied then this is the new address of the pc (pc + 4)
wire [31:0] branchSucessNewPC = immediateB + pcIn;//if a branch condition is satasfied then this is the new address of the pc 

/* Output Logic */

always @*
begin
	case (operation)
		2'b00://jal
		begin
			newPCOut = immediateJ + pcIn;
			rdToOutput = jumpRD;
			invalidFunct3ToOutput = 1'b0;//funct3 is not present for jal
		end
		2'b01://jalr
		begin
			newPCOut = immediateI + portRS1;
			rdToOutput = jumpRD;
			invalidFunct3ToOutput = (funct3 == 3'b000) ? 1'b0 : 1'b1;//funct3 should be 3'b000
		end
		2'b10://branch instructions
		begin
			//Based on if branch is taken or not, either output new pc or just increment the pc by 4
			newPCOut = branchTaken ? branchSucessNewPC : branchFailNewPC;
			
			rdToOutput = 32'hxxxxxxxx;//Branch instructions do not change rd; control logic should not latch this
			
			if ((funct3 == 3'b010) || (funct3 == 3'b011))//invalid funct3
				invalidFunct3ToOutput = 1'b1;
			else
				invalidFunct3ToOutput = 1'b0;
		end
		2'b11://pc increment
		begin
			newPCOut = nextInstruction;
			rdToOutput = 32'h00000000;//no need to write to main registers; forcing this to 0 allows other modules to write to registers concurently
			invalidFunct3ToOutput = 1'b0;//not an instruction, so we don't have a funct3
		end
	endcase
end

/* Branch Comparisons */

always @*
begin
	case (funct3)//Todo implement myself for practice instead of relying on verilog operators
		3'b000: branchTaken = portRS1 == portRS2;//beq
		3'b001: branchTaken = portRS1 != portRS2;//bne
		3'b100: branchTaken = $signed(portRS1) < $signed(portRS2);//blt
		3'b101: branchTaken = $signed(portRS1) >= $signed(portRS2);//bge
		3'b110: branchTaken = portRS1 < portRS2;//bltu
		3'b111: branchTaken = portRS1 >= portRS2;//bgeu
		default: branchTaken = 1'bx;//bad funct3 or not a branch instruction
	endcase
end

endmodule
