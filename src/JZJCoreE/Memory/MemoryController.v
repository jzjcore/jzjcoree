module MemoryController//Set control lines and addresses on a negedge, and MemoryController will output them on the immediatly following posedge
#(
	parameter INITIAL_CONTENTS = "initialRam.mem",
	parameter A_WIDTH = 12
)
(
	input clock,
	input reset,
	
	//Data Outputs
	output [31:0] dataOut,
	output reg unalignedAccess,
	output reg invalidFunct3,
	
	//Instruction stuff
	output [31:0] instructionOut,
	input [31:0] instructionAddress,
	
	//Operation control lines
	//Note: for partial memory writes (only a byte or a halfword), the operation must be 1 for a posedge then switch to 2; for whole word writes 2 can just be used immediatly
	input [1:0] memOperationType,//0 indicates a load instruction, 1 indicates a read from memory to be modified for a write instruction, 2 indicates actually executing the write instruction, and 3 is invalid
	input [2:0] funct3,//used to deside between lw/lh/lhu/lb/lbu if memOperationType == 0, or between sw/sh/sb if memOperationType == 1
	input enable,//output enable for a memOperationType of 0 or 2, write enable for a memOperationType of 1
	
	//Data sources (for pc and register data to be used to access memory addresses)
	input [31:0] portRS1,
	input [31:0] portRS2,//rs2
	input [31:0] immediateI,//offset for loads
	input [31:0] immediateS,//offset for stores
	
	//CPU memory mapped ports
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portXInput[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portXOutput[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Reads from the address read from the input, writes write to the output
	//Inputs: (byte-wise read)		address (starting byte)
	input [31:0] portAInput,//		FFFFFFE0
	input [31:0] portBInput,//		FFFFFFE4
	input [31:0] portCInput,//		FFFFFFE8
	input [31:0] portDInput,//		FFFFFFEC
	input [31:0] portEInput,//		FFFFFFF0
	input [31:0] portFInput,//		FFFFFFF4
	input [31:0] portGInput,//		FFFFFFF8
	input [31:0] portHInput,//		FFFFFFFC
	//Outputs: (word-wise write)	address (starting byte)
	output [31:0] portAOutput,//	FFFFFFE0
	output [31:0] portBOutput,//	FFFFFFE4
	output [31:0] portCOutput,//	FFFFFFE8
	output [31:0] portDOutput,//	FFFFFFEC
	output [31:0] portEOutput,//	FFFFFFF0
	output [31:0] portFOutput,//	FFFFFFF4
	output [31:0] portGOutput,//	FFFFFFF8
	output [31:0] portHOutput//	FFFFFFFC
	//For tristate ports, an additional port's outputs can be designated as a direction register, which can be used by and external module to allow/disalow writing
	//If feedback is desired, then inputs should be connected to their respective output register
	//MAKE SURE INPUTS ARE SYNCHRONIZED IF THEY ARE FROM ANOTHER CLOCK DOMAIN
);
/* Wires, Registers, and Assignments */

assign dataOut = (enable & (memOperationType == 2'b00)) ? processedDataOut : 32'h00000000;//Only output data for reading operations that need to output and when enabled

reg okToWrite;
wire backendWriteEnable = okToWrite & enable & (memOperationType == 2'b10);//only writing for store operations and when enabled and when valid

//Regular data stuff
reg [31:0] processedDataIn;//data processed for passing to backend
reg [31:0] processedDataOut;//data processed out of backend
wire [31:0] rawDataOut;//raw data from backend

//Address stuff
reg [31:0] byteWiseAddress;
wire [29:0] backendAddress;//30 bits because backend is word wise
wire [1:0] offset;

//Instruction stuff
wire [31:0] rawInstructionOut;
assign instructionOut = toBigEndian(rawInstructionOut);
wire [29:0] backendInstructionAddress = instructionAddress[31:2];//offset is ignored because it must be 0 otherwise pc will halt cpu

/* Address Source Selection, Byte-wise to Word-wise Translation, and Offset Calculation */

wire [31:0] loadAddress = portRS1 + immediateI;
wire [31:0] storeAddress = portRS1 + immediateS;

always @*
begin
	case (memOperationType)//Note: there are divisions by 4 to convert to byte-wise address; partial word accesses are handled in Data Processing and Misalignment Detection Logic
		2'b00: byteWiseAddress = loadAddress;//load instruction
		2'b01: byteWiseAddress = storeAddress;//read of data before store instruction (same address as it)
		2'b10: byteWiseAddress = storeAddress;//store instruction
		2'b11: byteWiseAddress = 32'hxxxxxxxx;//not a valid memOperationType
	endcase
end

assign backendAddress = byteWiseAddress[31:2];
assign offset = byteWiseAddress[1:0];

/* Data Processing and Misalignment Detection Logic */
//Sets okToWrite, invalidFunct3, processedDataIn, unalignedAccess, and processedDataOut based on memOperationType, funct3, offset, and rawDataOut

always @*
begin
	if (enable)
	begin
		case (memOperationType)
			2'b00://load instruction
			begin
				okToWrite = 1'b0;//not writing to memory
				processedDataIn = 32'hxxxxxxxx;//not writing to memory
			
				case (funct3)
					3'b000://lb
					begin
						processedDataOut = signExtend8To32(getByteAtOffset(rawDataOut, offset));//load byte
						unalignedAccess = 1'b0;//can't be unaligned with byte wise accesses
					end
					3'b001://lh
					begin
						processedDataOut = signExtend16To32(toBigEndian16(getHWAtOffset(rawDataOut, offset)));//load halfword
						unalignedAccess = (offset == 2'b01) || (offset == 2'b11);
					end
					3'b010://lw
					begin
						processedDataOut = toBigEndian(rawDataOut);
						unalignedAccess = offset != 2'b00;
					end
					3'b100://lbu
					begin
						processedDataOut = zeroExtend8To32(getByteAtOffset(rawDataOut, offset));//load byte
						unalignedAccess = 1'b0;//can't be unaligned with byte wise accesses
					end
					3'b101://lhu
					begin
						processedDataOut = zeroExtend16To32(toBigEndian16(getHWAtOffset(rawDataOut, offset)));//load halfword
						unalignedAccess = (offset == 2'b01) || (offset == 2'b11);
					end
					default:
					begin
						processedDataOut = 32'hxxxxxxxx;//invalid funct3
						unalignedAccess = 1'b0;//can't tell if we have an unaligned access because we have a bad funct3
					end
				endcase
				
				if ((funct3 == 3'b011) || (funct3 == 3'b110) || (funct3 == 3'b111))//invalid funct3
					invalidFunct3 = 1'b1;
				else
					invalidFunct3 = 1'b0;
			end
			2'b01://read to be modified and written by store instruction on next posedge
			begin
				invalidFunct3 = 1'b0;//there is no funct3 to be invalid
				okToWrite = 1'b0;//not writing to memory
				processedDataIn = 32'hxxxxxxxx;//not writing to memory
				processedDataOut = 32'hxxxxxxxx;//don't need to convert/output anything because 2'b10 will just use rawDataOut directly and internally next clock cycle
				unalignedAccess = 1'b0;//accessing a whole word to modify on next posedge; if the address we are accessing is unaligned we'll deal with it in 2'b10
			end
			2'b10://write instruction (either immediatly for whole word writes or after 2'b01)
			begin
				processedDataOut = 32'hxxxxxxxx;//not reading from memory
			
				case (funct3)
					3'b000://sb
					begin//After 2'b01, current data at address is stored in rawDataOut
						processedDataIn = replaceByteAtOffset(rawDataOut, portRS2[7:0], offset);
						okToWrite = 1'b1;//can't be unaligned with byte wise accesses
						unalignedAccess = 1'b0;//can't be unaligned with byte wise accesses
						invalidFunct3 = 1'b0;
					end
					3'b001://sh
					begin//After 2'b01, current data at address is stored in rawDataOut
						processedDataIn = replaceHWAtOffset(rawDataOut, toLittleEndian16(portRS2[15:0]), offset);
						unalignedAccess = (offset == 2'b01) || (offset == 2'b11);
						okToWrite = !unalignedAccess;//only write if access is aligned
						invalidFunct3 = 1'b0;
					end
					3'b010://sw
					begin
						processedDataIn = toLittleEndian(portRS2);
						okToWrite = offset == 2'b00;
						unalignedAccess = offset != 2'b00;
						invalidFunct3 = 1'b0;
					end
					default:
					begin
						okToWrite = 1'b0;//bad funct3; not writing to memory
						unalignedAccess = 1'b0;//don't know because bad funct3
						processedDataIn = 32'hxxxxxxxx;//bad funct3; not writing to memory
						invalidFunct3 = 1'b1;
					end
				endcase
			end
			2'b11://invalid memOperationType (should never happen)
			begin
				invalidFunct3 = 1'bx;//invalid memOperationType; anything goes
				okToWrite = 1'bx;//invalid memOperationType; anything goes
				processedDataIn = 32'hxxxxxxxx;//invalid memOperationType; anything goes
				unalignedAccess = 1'bx;//invalid memOperationType; anything goes
				processedDataOut = 32'hxxxxxxxx;//invalid memOperationType; anything goes
			end
		endcase
	end
	else//disabled
	begin
		invalidFunct3 = 1'b0;//not invalid because module is not enabled
		okToWrite = 1'b0;//not writing to memory
		unalignedAccess = 1'b0;//access is not unaligned because we're not accessing memory
		processedDataIn = 32'hxxxxxxxx;//not writing to memory
		processedDataOut = 32'hxxxxxxxx;//not reading from memory
	end
end

/* Byte And Half Word Selection/Manipulation Functions */

function automatic [31:0] getByteAtOffset (input [31:0] word, input [1:0] offset);
begin
	case (offset)
		2'b00: getByteAtOffset = word[31:24];
		2'b01: getByteAtOffset = word[23:16];
		2'b10: getByteAtOffset = word[15:8];
		2'b11: getByteAtOffset = word[7:0];
	endcase
end
endfunction

function automatic [31:0] getHWAtOffset (input [31:0] word, input [1:0] offset);
begin
	case (offset)
		2'b00: getHWAtOffset = word[31:16];
		2'b01: getHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
		2'b10: getHWAtOffset = word[15:0];
		2'b11: getHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
	endcase
end
endfunction

function automatic [31:0] replaceByteAtOffset (input [31:0] word, input [7:0] newByte, input [1:0] offset);
begin
	case (offset)
		2'b00: replaceByteAtOffset = {newByte, word[23:0]};
		2'b01: replaceByteAtOffset = {word[31:24], newByte, word[15:0]};
		2'b10: replaceByteAtOffset = {word[31:16], newByte, word[7:0]};
		2'b11: replaceByteAtOffset = {word[31:8], newByte};
	endcase
end
endfunction

function automatic [31:0] replaceHWAtOffset (input [31:0] word, input [15:0] newHW, input [1:0] offset);
begin
	case (offset)
		2'b00: replaceHWAtOffset = {newHW, word[15:0]};
		2'b01: replaceHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
		2'b10: replaceHWAtOffset = {word[31:16], newHW};
		2'b11: replaceHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
	endcase
end
endfunction

/* External Stuff */

//Endianness functions
`include "JZJCoreE/Memory/EndiannessFunctions.v"

//Bit extension functions
`include "JZJCoreE/BitExtensionFunctions.v"

//Memory backend
MemoryBackend #(.INITIAL_RAM_CONTENTS(INITIAL_CONTENTS), .A_WIDTH(A_WIDTH)) memoryBackend
					(.clock(clock), .reset(reset), .address(backendAddress), .dataIn(processedDataIn), .writeEnable(backendWriteEnable), .dataOut(rawDataOut), .instructionOut(rawInstructionOut),
					 .instructionAddress(backendInstructionAddress), .portAInput(portAInput), .portBInput(portBInput), .portCInput(portCInput), .portDInput(portDInput), .portEInput(portEInput),
					 .portFInput(portFInput), .portGInput(portGInput), .portHInput(portHInput), .portAOutput(portAOutput), .portBOutput(portBOutput), .portCOutput(portCOutput),
					 .portDOutput(portDOutput), .portEOutput(portEOutput), .portFOutput(portFOutput), .portGOutput(portGOutput), .portHOutput(portHOutput));

endmodule