module MemoryMappedIOManager
(
	input clock,
	input reset,
	
	//Addressing
	input [29:0] address,
	
	//IO
	input [31:0] dataIn,
	output [31:0] dataOut,
	input writeEnable,
	
	//CPU memory mapped ports
	//Reads from the address read from the input, writes write to the output
	//Inputs: (word-wise read)			address
	input [31:0] portAInput,//			3FFFFFF8
	input [31:0] portBInput,//			3FFFFFF9
	input [31:0] portCInput,//			3FFFFFFA
	input [31:0] portDInput,//			3FFFFFFB
	input [31:0] portEInput,//			3FFFFFFC
	input [31:0] portFInput,//			3FFFFFFD
	input [31:0] portGInput,//			3FFFFFFE
	input [31:0] portHInput,//			3FFFFFFF
	//Outputs: (word-wise write)		address
	output reg [31:0] portAOutput,//	3FFFFFF8
	output reg [31:0] portBOutput,//	3FFFFFF9
	output reg [31:0] portCOutput,//	3FFFFFFA
	output reg [31:0] portDOutput,//	3FFFFFFB
	output reg [31:0] portEOutput,//	3FFFFFFC
	output reg [31:0] portFOutput,//	3FFFFFFD
	output reg [31:0] portGOutput,//	3FFFFFFE
	output reg [31:0] portHOutput//	3FFFFFFF
	//For tristate ports, an additional port's outputs can be designated as a direction register, which can be handled by and external module
	//If feedback is desired, then inputs should be connected to their respective output register
	//MAKE SURE INPUTS ARE SYNCHRONIZED IF THEY ARE FROM ANOTHER CLOCK DOMAIN
);
/* Wires, Registers, and Assignments */

wire [31:0] bigEndianDataIn = toBigEndian(dataIn);
reg [31:0] bigEndianDataOut;
assign dataOut = toLittleEndian(bigEndianDataOut);//Output logic will output 0 if not in bounds so as to not interfere with other memory accesses

/* Output Logic */

always @*
begin
	casex (address)//Since the memory map is very simple, don't cares are fine here (all we have defined is registers at 3FFFFFF*, it dosen't not matter if they are mirrored a bunch)
		30'h3xxxxxx8: bigEndianDataOut = portAInput;
		30'h3xxxxxx9: bigEndianDataOut = portBInput;
		30'h3xxxxxxA: bigEndianDataOut = portCInput;
		30'h3xxxxxxB: bigEndianDataOut = portDInput;
		30'h3xxxxxxC: bigEndianDataOut = portEInput;
		30'h3xxxxxxD: bigEndianDataOut = portFInput;
		30'h3xxxxxxE: bigEndianDataOut = portGInput;
		30'h3xxxxxxF: bigEndianDataOut = portHInput;
		default: bigEndianDataOut = 32'h00000000;//not in bounds, don't output anything
	endcase
end

/* Writing Logic */
always @(posedge clock, posedge reset)
begin
	if (reset)
		initRegisters();
	else if (clock)
	begin
		if (writeEnable)
		begin
			casex (address)
				30'h3xxxxxx8: portAOutput <= bigEndianDataIn;
				30'h3xxxxxx9: portBOutput <= bigEndianDataIn;
				30'h3xxxxxxA: portCOutput <= bigEndianDataIn;
				30'h3xxxxxxB: portDOutput <= bigEndianDataIn;
				30'h3xxxxxxC: portEOutput <= bigEndianDataIn;
				30'h3xxxxxxD: portFOutput <= bigEndianDataIn;
				30'h3xxxxxxE: portGOutput <= bigEndianDataIn;
				30'h3xxxxxxF: portHOutput <= bigEndianDataIn;
				default:
				begin
					//do nothing because address is not in bounds
				end
			endcase
		end
	end
end

/* Functions */

//Endianness functions
`include "JZJCoreE/Memory/EndiannessFunctions.v"

/* Register Initialization */

initial
begin
	initRegisters();
end

//Set all io dir registers to 0 and set all port registers to z
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and reset inside of the write block
begin
	//Zero all port registers
	portAOutput <= 32'h00000000;
	portBOutput <= 32'h00000000;
	portCOutput <= 32'h00000000;
	portDOutput <= 32'h00000000;
	portEOutput <= 32'h00000000;
	portFOutput <= 32'h00000000;
	portGOutput <= 32'h00000000;
	portHOutput <= 32'h00000000;
end
endtask

endmodule
