module TriplePortSynchronousRam//Based on old sync_ram module
#(
	parameter INITIALIZE_FROM_FILE = 0,//whether to have default ram contents at boot
	parameter FILE = "rom.mem",
	parameter FILE_TYPE_BIN = 0,//hex by default
	parameter D_WIDTH = 8,
	parameter A_WIDTH = 8
)
(
	input clock,
	
	input [D_MAX:0] dataIn,
	input [A_MAX:0] writeAddress,
	input writeEnable,
	
	output reg [D_MAX:0] dataOutA,
	input [A_MAX:0] readAddressA,
	
	output reg [D_MAX:0] dataOutB,
	input [A_MAX:0] readAddressB
);
//based on https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/qts/qts_qii51007.pdf

localparam D_MAX = D_WIDTH - 1;
localparam A_MAX = A_WIDTH - 1;
localparam NUM_OF_ADDRESSES = 2 ** A_WIDTH;
localparam NUM_OF_ADDRESSES_MAX = NUM_OF_ADDRESSES - 1;

reg [D_MAX:0] ram [NUM_OF_ADDRESSES_MAX:0];

initial
begin
	if (INITIALIZE_FROM_FILE)
	begin
		if (FILE_TYPE_BIN)
		  $readmemb(FILE, ram);
		else
		  $readmemh(FILE, ram);
	end
end

always @(posedge clock)
begin
	if (writeEnable)
		ram[writeAddress] <= dataIn;
	
	//will have old values until next clock cycle
	dataOutA <= ram[readAddressA];
	dataOutB <= ram[readAddressB];
end

endmodule