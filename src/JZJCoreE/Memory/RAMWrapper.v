module RAMWrapper//Wraps the sync_ram module to only respond to accesses to addresses in its address space
#(
	parameter INITIAL_RAM_CONTENTS = "initialRam.mem",
	parameter A_WIDTH = 12
)
(
	input clock,
	
	//Addressing
	input [29:0] address,
	
	//IO
	input [31:0] dataIn,
	output [31:0] dataOut,
	input writeEnable,
	
	//Instruction stuff
	output [31:0] instructionOut,
	input [29:0] instructionAddress
);
/* Parameters */

//Address bounds (wordwise and inclusive)
localparam MIN_RAM_ADDRESS = 30'h00000000;//Starts at byte and word address 0
localparam MAX_RAM_ADDRESS = (2 ** A_WIDTH) - 1;//4096 words by default (16KiB = 16384 bytes total, therefore last address is from 16380 to 16383 (word-wise address 4095))
localparam MAX_RAM_ADDRESS_PLUS_ONE = MAX_RAM_ADDRESS + 1;//allows use of a less than instead of a less than or equal to

/* Wires */

//wire isInBounds = (address >= MIN_RAM_ADDRESS) & (address <= MAX_RAM_ADDRESS);
//wire isInBounds = (address < MAX_RAM_ADDRESS_PLUS_ONE);//This is equivalent to the above but because synthesis tools symplify the above to this anyway, the above is more readable and flexible
wire isInBounds = ~address[29];//Any address greater than 30'h20000000 is not in bounds; I doubt any fpga has 2gigs of sram lol, so this is fine for any sane value of A_WIDTH
wire actualWriteEnable;
wire [31:0] directDataOut;

/* Enable logic */

assign actualWriteEnable = writeEnable & isInBounds;//only enable write enable if addresses are in ram address space
assign dataOut = isInBounds ? directDataOut : 30'h00000000;//only output data if addresses are in ram address space

/* Triple port ram module */

TriplePortSynchronousRam #(.FILE(INITIAL_RAM_CONTENTS), .A_WIDTH(A_WIDTH), .D_WIDTH(32), .INITIALIZE_FROM_FILE(1)) ram
								  (.clock(clock), .writeEnable(actualWriteEnable), .dataOutA(directDataOut), .readAddressA(address), .dataIn(dataIn), .writeAddress(address),
									.dataOutB(instructionOut), .readAddressB(instructionAddress));

endmodule