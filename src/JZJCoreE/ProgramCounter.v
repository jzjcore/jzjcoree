module ProgramCounter
(
	input clock,
	input reset,
	
	//PC updating
	input writeEnable,
	input [31:0] newPCIn,
	
	//Output
	output [31:0] pcOut,//contains the data that is currently in the pc
	output misaligned//program counter is not aligned to a 4 byte address
);
/* PC Register */

reg [31:0] actualProgramCounter = 32'h00000000;//start at address 0x00000000

/* Output Logic */

assign pcOut = actualProgramCounter;
assign misaligned = actualProgramCounter[1:0] != 2'b00;//program counter is not aligned to a 4 byte address

/* Writing Logic */

always @(posedge clock, posedge reset)
begin
	if (reset)
		actualProgramCounter <= 32'h00000000;//start at address 0x00000000
	else if (clock)
	begin
		if (writeEnable)
			actualProgramCounter <= newPCIn;
	end
end

endmodule