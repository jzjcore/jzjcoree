//Note: This is not part of the core, just a file for me to use to test the core on my devboard
//To actually use the core, just add JZJCoreE.v and its dependencies to your project (everything in the JZJCoreE folder)
module TopTestingFile//heavily borrowed from JZJCoreC
(
	input clock,//50mhz
	input notReset,

	//Testing things I have setup
	input [3:0] notButton,
	output [3:0] notLed,
	output [7:0] logicAnalyzerOut,
	//7 segment display
	output [7:0] segment,
	output [3:0] digit
);
//Inversion for inverted devboard stuff
wire reset = ~notReset;
wire [3:0] button = ~notButton;
wire [3:0] led;
assign notLed = ~led;

//Clock stuff division stuff
reg [18:0] clockPrescaler = 19'h00000;
always @(posedge clock)
begin
	clockPrescaler <= clockPrescaler + 19'h00001;
end

wire clock25MHz = clockPrescaler[0];//25mhz (50mhz / (2^1)) (just under fmax)
wire clock90Hz = clockPrescaler[18];//about 90 hz (50mhz / (2^19)) (for debugging)

//Wires
wire [31:0] register31Output;

assign logicAnalyzerOut[7] = clock90Hz;
assign logicAnalyzerOut[6:0] = register31Output;
wire [31:0] portEOutput;
wire [31:0] portEInput;
wire [31:0] portBOutput;

//Port stuffs
assign portEInput[3:0] = button[3:0];//Todo this really should be synchronized before passion to the core
assign led[3:0] = portEOutput[7:4];
assign portEInput[7:4] = portEOutput[7:4];//Feedback

wire [15:0] displayOutput;
//assign displayOutput = portBOutput[15:0];
assign displayOutput = register31Output;

//The core
localparam FILE = "memFiles/memorymappediowritetest.mem";

//Full speed
//JZJCoreE #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
//(.clock(clock), .reset(reset), .register31Output(register31Output), .portEOutput(portEOutput), .portEInput(portEInput), .portBOutput(portBOutput));

//Half speed
//JZJCoreE #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
//(.clock(clock25MHz), .reset(reset), .register31Output(register31Output), .portEOutput(portEOutput), .portEInput(portEInput), .portBOutput(portBOutput));

//Slow
JZJCoreE #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
(.clock(clock90Hz), .reset(reset), .register31Output(register31Output), .portEOutput(portEOutput), .portEInput(portEInput), .portBOutput(portBOutput));

//7 segment display output
multi7seg (.clock(clockPrescaler[17]), .data0(displayOutput[15:12]), .data1(displayOutput[11:8]), .data2(displayOutput[7:4]), .data3(displayOutput[3:0]), .segment(segment), .ground(digit));

endmodule