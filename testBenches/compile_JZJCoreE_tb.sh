cd ../src
iverilog -o /tmp/JZJCoreE.vvp ../testBenches/JZJCoreE_tb.v JZJCoreE/ALU.v JZJCoreE/ControlLogic.v JZJCoreE/InstructionDecoder.v JZJCoreE/JZJCoreE.v JZJCoreE/PCALU.v JZJCoreE/ProgramCounter.v JZJCoreE/RegisterFile.v JZJCoreE/ValueFormer.v JZJCoreE/Memory/MemoryBackend.v JZJCoreE/Memory/MemoryController.v JZJCoreE/Memory/TriplePortSynchronousRam.v JZJCoreE/Memory/MemoryMappedIOManager.v JZJCoreE/Memory/RAMWrapper.v
vvp /tmp/JZJCoreE.vvp
gtkwave /tmp/JZJCoreE.vcd
