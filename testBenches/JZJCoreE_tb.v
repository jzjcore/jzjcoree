`timescale 1ns/1ps
module JZJCoreE_tb;

reg clock = 1'b0;

JZJCoreE #(.INITIAL_MEM_CONTENTS("../src/memFiles/memorymappediowritetest.mem")) coreTest (.clock(clock), .reset(1'b0));

always
begin
    #10;
    clock = ~clock;
end

initial
begin
    $dumpfile("/tmp/JZJCoreE.vcd");
    $dumpvars(0,JZJCoreE_tb);
    #10000 $finish;
end

endmodule
